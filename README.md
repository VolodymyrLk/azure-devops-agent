# AzDevOpsAgent

## Prerequisites

0. Virtual Box and Vargant must be installed

0. ```vagrant global-status -p``` will tell you the state of all active Vagrant environments on the system for the currently logged in user

```
id       name    provider   state   directory
---------------------------------------------------------------------------------------------
aber558  ansible virtualbox running C:/Users/../../general/ansible/ubuntu1604ansible
```

0. ```vagrant box list``` lists all the boxes that are installed into Vagrant.
```
ubuntu/xenial64 (virtualbox, 20190724.1.0)
```

## Build agent machine deployment

0. Navigate to Vagrantfile directory
```cd ubuntu1604ansible/```

0. ```vagrant box update```

0. ```vagrant up```

0. Run test Ansible Playbook
```
vagrant ssh
ansible-galaxy install yohanb.azure_devops_agent
ansible-playbook /vagrant/azdevopsagent/ubuntu1604ansible/playbook.yml
```

0. vars.yml
```
---

az_devops_accountname: "........."
az_devops_accesstoken: "........."
az_devops_project_name: "........."
az_devops_deployment_group_name: ""
az_devops_agent_pool_name: "Ansible"
```